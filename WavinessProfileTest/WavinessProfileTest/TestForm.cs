﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace WavinessProfileTest
{
    public partial class TestForm : Form
    {
        profile algorithm = new profile();
        string filePath = "C:\\Users\\SK\\Desktop\\binary\\binary";
        Bitmap loadImage;
        List<Bitmap> listImage = new List<Bitmap>();
        string selectFilePath = "";

        public TestForm()
        {
            InitializeComponent();
        }

        public void MakeShapeImage()
        {
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            algorithm.LoadCsvDataFile(selectFilePath,182, 301);
            algorithm.Normalize();
            loadImage = algorithm.MakeShapeImage();
            stopwatch.Stop();
            //MessageBox.Show(stopwatch.ElapsedMilliseconds.ToString());
        }

        private Bitmap LoadImageFromFile(string filePath)
        {
            Bitmap loadedImage = null;
            FileStream stream = null;
            try
            {
                stream = File.OpenRead(filePath);
                MemoryStream memoryStream = new MemoryStream();
                byte[] buffer = new byte[10000];
                while (true)
                {
                    int read = stream.Read(buffer, 0, 10000);
                    if (read == 0) break;
                    memoryStream.Write(buffer, 0, read);
                }
                loadedImage = (Bitmap)Bitmap.FromStream(memoryStream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
            return loadedImage;
        }

        private int ReadFile()
        {
            int result = -1;

            do
            {
                DirectoryInfo imageDirInfo = new DirectoryInfo(filePath);
                if (!imageDirInfo.Exists)
                {
                    if (MessageBox.Show("'" + filePath + "'" + "가 없습니다.") == DialogResult.OK)
                        break;
                }
                FileInfo[] imageInfo = imageDirInfo.EnumerateFiles().ToArray();
                
                for (int index = 0; index < imageInfo.Length; ++index)
                {
                    listImage.Add(LoadImageFromFile(imageInfo[index].FullName));
                }
            } while (false);

            return result;
        }

        private void button_selectFile_Click(object sender, EventArgs e)
        {
            if(SelectFile(".jpg") == 0)
            {
                loadImage = new Bitmap(selectFilePath);
            }
            else
                MessageBox.Show("Image Load Error");
        }

        private int SelectFile(string defaultExt)
        {
            int result = -1;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = defaultExt;
            ofd.Filter = "(*" + defaultExt+")|*" + defaultExt;
            
            ofd.ShowDialog();
            if (ofd.FileName.Length > 0)
            {
                selectFilePath = ofd.FileName;
                textBox_imagePath.Text = selectFilePath;
                result = 0;
            }

            return result;
        }

        private void SelectFolder()
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            System.Windows.Forms.DialogResult res = folderDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                selectFilePath = folderDialog.SelectedPath;
                textBox_imagePath.Text = selectFilePath;
            }
        }

        private void DrawProfile(byte[,] data)
        {
            Graphics graphic = CreateGraphics();
            graphic.Clear(SystemColors.Control);
            int stptX = 200;
            int stptY = 300;
            for (int i = 0; i < data.Length - 1; ++i)
            {
                graphic.DrawLine(Pens.Black, new PointF(stptX + (i * (float)0.5), stptY - (data[i, 0])), new PointF(stptX + ((i + 1) * (float)0.5), stptY - (data[i + 1, 0])));
            }
        }

        private void button_makeImage_Click(object sender, EventArgs e)
        {
            if (SelectFile(".csv") == 0)
            {
                MakeShapeImage();
            }
            else
                MessageBox.Show(".csv File Load Error");
        }

        private void button_Profile_Click(object sender, EventArgs e)
        {
            try
            {
                List<byte[,]> listProfileData;
                double degree = Convert.ToDouble(textBox_degree.Text);
                int lineNum = Convert.ToInt32(textBox_numLines.Text);
                algorithm.cvGetProfile(loadImage, degree, lineNum, out listProfileData);
                for (int index = 0; index < listProfileData.Count; ++index)
                {
                    algorithm.DrawProfile(index.ToString(), listProfileData[index]);
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }

        private void button_Hough_Click(object sender, EventArgs e)
        {
            listImage.Clear();
            algorithm.Binarization(loadImage, out listImage);
            algorithm.HoughLinesTest(listImage, selectFilePath);
        }
    }
}
