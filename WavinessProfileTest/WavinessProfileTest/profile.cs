﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Windows.Forms;

namespace WavinessProfileTest
{
    class profile
    {
        double[,] matData;
        double valueMax;
        double valueMin;
        int dataWidth;
        int dataHeight;
        int imageSize = 310;

        #region .csv 파일로 이미지 만들기
        public unsafe void LoadCsvDataFile(string filename, int xdim, int ydim)
        {
            do
            {
                if (File.Exists(filename) == false)
                    break;
                
                valueMax = double.MinValue;
                valueMin = double.MaxValue;
                dataWidth = ydim;
                dataHeight = xdim;

                using (TextReader reader = File.OpenText(filename))
                {
                    string data = reader.ReadToEnd();
                    string[] stringSeparators = { ",", "\r\n" };
                    string[] intstrs = data.Split(stringSeparators, StringSplitOptions.None);
                    int inx = 0;
                    int mCount = 0;
                    matData = new double[xdim, ydim];

                    for (int y = 0; y<ydim; ++y)
                    {
                        for(int x=0; x<xdim; ++inx)
                        {
                            if (intstrs[inx] == "-" || intstrs[inx] == "") intstrs[inx] = "9999";
                            if (inx <= 198) continue;
                            if (((inx - 198) % 183) == 0)
                            { continue; }
                            double val = Convert.ToDouble(intstrs[inx]);
                            matData[x, y] = val;

                            if (matData[x, y] < 0)
                                mCount++;
                            if (x > 0 && matData[x, y] != 9999 && matData[x, y] > valueMax)
                                valueMax = matData[x, y];
                            if (x > 0 && matData[x, y] != 9999 && matData[x, y] < valueMin)
                                valueMin = matData[x, y];
                            x++;
                        }
                    }

                    reader.Close();
                }

            } while (false);
        }

        public void Normalize()
        {
            for (int y = 0; y < dataWidth; ++y)
            {
                for (int x = 0; x < dataHeight; ++x)
                {
                    if (matData[x, y] == 9999)
                    {
                        matData[x, y] = 0;
                        continue;
                    }
                    matData[x, y] = ((matData[x, y] - valueMin) / (valueMax - valueMin)) * 245 + 10; //10~255
                }
            }
        }

        public unsafe Bitmap MakeShapeImage()
        {
            Rectangle aImgRect = new Rectangle(0, 0, imageSize, imageSize);
            Bitmap imageData = new Bitmap(imageSize, imageSize, PixelFormat.Format24bppRgb);
            BitmapData bmpData = imageData.LockBits(aImgRect, ImageLockMode.ReadOnly, imageData.PixelFormat);
            IntPtr dst = bmpData.Scan0;
            byte* SrcBmp = (byte*)dst.ToPointer();

            int nidx = 0;
            int mapindex = 0;
            float angleRange = 1;
            float mag = 6;
            double centerPoint = imageSize / 2;
            float radian = 0;
            double dx = 0;
            double dy = 0;
            float angleVariable = 0;
            float interpolationVariable = 0;

            for (int degree = 0; degree < 180; ++degree)
            {
                for(float subDegree = 0; subDegree < angleRange * mag; subDegree++) //1도 사이를 1/mag로 나누어 data 채우기
                {
                    angleVariable = (1 / (mag)) * subDegree; //각도 변화량
                    interpolationVariable = (1 / (angleRange * mag)) * subDegree; //보간, data 변화량
                    mapindex = 0;
 
                    for (int length = -150; length < 150; ++length, ++mapindex) 
                    {
                        if(length <= 0)
                            radian = (float)((degree + angleVariable) * Math.PI / 180);
                        else
                            radian = (float)((degree + 180 + angleVariable) * Math.PI / 180);

                        dx = Math.Round(centerPoint - (Math.Abs(length) * Math.Cos(radian)));
                        dy = Math.Round(centerPoint + (Math.Abs(length) * Math.Sin(radian)));
                        nidx = (int)((dx * bmpData.Stride) + dy * 3);

                        SrcBmp[nidx] = (byte)(matData[degree, mapindex] + ((matData[degree + 1, mapindex] - matData[degree, mapindex]) * (interpolationVariable)));
                        SrcBmp[nidx + 1] = (byte)(matData[degree, mapindex] + ((matData[degree + 1, mapindex] - matData[degree, mapindex]) * (interpolationVariable)));
                        SrcBmp[nidx + 2] = (byte)(matData[degree, mapindex] + ((matData[degree + 1, mapindex] - matData[degree, mapindex]) * (interpolationVariable)));
                    }
                }
            }
            imageData.UnlockBits(bmpData);
            return imageData;
        }

        #region 주석
        /*
        public unsafe void MakeImage()
        {
            ///이미지 크기를 줄이면 profile 데이터도 비율에 맞게 가져와야됨 (datay)
            int imageSize = 746*2;
            Rectangle aImgRect = new Rectangle(0, 0, imageSize, imageSize);
            Bitmap imageData = new Bitmap(imageSize, imageSize, PixelFormat.Format24bppRgb);
            BitmapData bmpData = imageData.LockBits(aImgRect, ImageLockMode.ReadOnly, imageData.PixelFormat);
            IntPtr dst = bmpData.Scan0;
            byte* SrcBmp = (byte*)dst.ToPointer();


            int nidx = 0;
            float angleRange = 5;
            float mag = 25;

            for (int x=0; x<(360/5); ++x)
            {
                for (float i = 0; i < angleRange*mag; i++)
                {
                    for (int y = 1, datay=1; y < dataHeight; ++y,datay+=1)  //5 = (746*2)/300 = (dataSize) / (imageSize)
                    {
                        float angleVariable = (1 / (mag)) * i;
                        float interpolationVariable = (1 / (angleRange * mag))*i; //보간
                        float radian = (float)(((x*5)+((angleVariable))) * Math.PI / 180);
                        double dx = Math.Round(imageSize / 2 - y * Math.Sin(radian));
                        double dy = Math.Round(imageSize / 2 + y * Math.Cos(radian));
                        nidx = (int)((dx * bmpData.Stride) + dy * 3);
                        if(x<71)
                        {
                            SrcBmp[nidx] = (byte)(matData[x, datay] + ((matData[x + 1, datay] - matData[x, datay]) * (interpolationVariable)));
                            SrcBmp[nidx + 1] = (byte)(matData[x, datay] + ((matData[x + 1, datay] - matData[x, datay]) * (interpolationVariable)));
                            SrcBmp[nidx + 2] = (byte)(matData[x, datay] + ((matData[x + 1, datay] - matData[x, datay]) * (interpolationVariable)));
                        }
                        else
                        {
                            SrcBmp[nidx] = (byte)(matData[x, datay] + ((matData[0, datay] - matData[x, datay]) * (interpolationVariable)));
                            SrcBmp[nidx + 1] = (byte)(matData[x, datay] + ((matData[0, datay] - matData[x, datay]) * (interpolationVariable)));
                            SrcBmp[nidx + 2] = (byte)(matData[x, datay] + ((matData[0, datay] - matData[x, datay]) * (interpolationVariable)));
                        }
                    }
                }
        
            }
          
            imageData.UnlockBits(bmpData);
        }
        */
        #endregion

        #endregion

        #region profile data 읽기
     
        public int cvGetProfile(Bitmap image, double angle, int numLines, out List<byte[,]> listProfileData)
        {
            int result = -1;
            listProfileData = new List<byte[,]>();
            do
            {
                try
                {
                    Image<Gray, byte> grayInputImage = new Image<Gray, byte>(image);
                    List<LineSegment2D> listProfileLine;
                    //profile line start, end point 계산
                    GetProfilePoint(grayInputImage.Size, angle, numLines, out listProfileLine);

                    for (int index = 0; index < listProfileLine.Count; ++index)
                    {
                        byte[,] profileData = grayInputImage.Sample(listProfileLine[index], Emgu.CV.CvEnum.Connectivity.EightConnected);
                        listProfileData.Add(profileData);
                    }
                    for (int index = 0; index < listProfileLine.Count; ++index)
                    {
                        grayInputImage.Draw(listProfileLine[index], new Gray(150), 1);
                        //grayInputImage.Draw(index.ToString(), new Point(listProfileLine[index].P1.X+15, listProfileLine[index].P1.Y+10), Emgu.CV.CvEnum.FontFace.HersheySimplex, 1, new MCvScalar(200))
                        //CvInvoke.PutText(grayInputImage, windowName, new Point(15, profileData[0, 0] + 15), Emgu.CV.CvEnum.FontFace.HersheySimplex, 1, new MCvScalar(200));
                    }
                    ShowImage("grayImage", grayInputImage.ToBitmap());
                }
                catch(Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
            } while (false);
            return result;
        }
         
        private int GetProfilePoint(Size imageSize, double angle, int numLines, out List<LineSegment2D> listprofileLine)
        {
            int result = -1;
            listprofileLine = new List<LineSegment2D>();
            do
            {
                Point startPt, endPt;
                double dx, dy;
                double centerX=imageSize.Width/2, centerY=imageSize.Height/2;
                float radian;
                int lineGap = (imageSize.Width) / (numLines+1);

                for(int index=0; index<numLines; ++index)
                {
                    radian = (float)((angle + 180) * Math.PI / 180);
                    if(angle == 0 || angle == 180)
                    {
                        centerX = imageSize.Width / 2;
                        centerY = lineGap * (index+1);
                    }
                    else if(angle == 90 || angle == 270)
                    {
                        centerX = lineGap * (index + 1);
                        centerY = imageSize.Height / 2;
                    }
                    else if((angle > 0 && angle < 90) || (angle > 180 && angle < 270))
                    {
                        centerX = lineGap * (index + 1);
                        centerY = lineGap * (index + 1);
                    } 
                    else if((angle > 90 && angle < 180) || (angle > 270 && angle < 360))
                    {
                        centerX = imageSize.Width - (lineGap * (index + 1));
                        centerY = lineGap * (index + 1);
                    }

                    dx = Math.Round(centerX - (Math.Abs(imageSize.Width / 2) * Math.Cos(radian)));
                    dy = Math.Round(centerY + (Math.Abs(imageSize.Height / 2) * Math.Sin(radian)));
                    startPt = new Point((int)dx, (int)dy);

                    radian = (float)((angle) * Math.PI / 180);
                    dx = Math.Round(centerX - (Math.Abs(imageSize.Width / 2) * Math.Cos(radian)));
                    dy = Math.Round(centerY + (Math.Abs(imageSize.Height / 2) * Math.Sin(radian)));
                    endPt = new Point((int)dx, (int)dy);

                    listprofileLine.Add(new LineSegment2D(startPt, endPt));
                    result = 0;
                }
            }
            while (false);
            return result;
        }
        
        public void DrawProfile(string windowName, byte[,] profileData)
        {
            try
            {
                Image<Gray, byte> profileImage = new Image<Gray, byte>(new Size(profileData.Length, 265));
                Point profilePt1, profilePt2;

                //draw grid
                for(int gridy = 0; gridy <= 255; gridy += 50)
                {
                    profilePt1 = new Point(0, 255 - gridy);
                    profilePt2 = new Point(profileImage.Width, 255 - gridy);
                    CvInvoke.Line(profileImage, profilePt1, profilePt2, new MCvScalar(100));
                }
                for(int gridx = 0; gridx <= profileImage.Width; gridx+=100)
                {
                    profilePt1 = new Point(gridx, 0);
                    profilePt2 = new Point(gridx, profileImage.Height);
                    CvInvoke.Line(profileImage, profilePt1, profilePt2, new MCvScalar(100));
                }

                //draw profile
                for (int location = 0; location < profileData.Length - 1; ++location)
                {
                    profilePt1 = new Point(location, 255 - profileData[location, 0] + 5);
                    profilePt2 = new Point(location + 1, 255 - profileData[location + 1, 0] + 5);
                    CvInvoke.Line(profileImage, profilePt1, profilePt2, new MCvScalar(200), 2);
                }
                
                CvInvoke.NamedWindow(windowName, Emgu.CV.CvEnum.NamedWindowType.AutoSize);
                CvInvoke.Imshow(windowName, profileImage);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void ShowImage(string windowName, Bitmap image)
        {
            try
            {
                CvInvoke.NamedWindow(windowName, Emgu.CV.CvEnum.NamedWindowType.AutoSize);
                CvInvoke.Imshow(windowName, new Image<Gray, byte>(image));
            }
            catch(Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }

        #region 주석
        /*
        public unsafe void GetProfile(Bitmap image, double degree)
        {
            Bitmap imageData = (Bitmap)image.Clone();//new Bitmap(image.Width, image.Height, PixelFormat.Format24bppRgb);
            BitmapData bmpData = imageData.LockBits(new Rectangle(0,0, imageData.Width, imageData.Height), ImageLockMode.ReadOnly, imageData.PixelFormat);
            IntPtr dst = bmpData.Scan0;
            byte* srcBmp = (byte*)dst.ToPointer();
            
            float radian = (float)(degree * Math.PI / 180);
            List<int> listData = new List<int>();
            //degree = 135;
            for (int radius= image.Width / 2; radius > 1; --radius)
            {
                double dx = Math.Round(imageData.Width / 2 - radius * Math.Sin(radian));
                double dy = Math.Round(imageData.Width / 2 + radius * Math.Cos(radian));
                int nidx = (int)((dx * bmpData.Stride) + dy * 3);

                listData.Add(srcBmp[nidx]);
            }

            radian = (float)((degree+180) * Math.PI / 180);
            for (int radius = 0; radius < image.Width / 2; ++radius)
            {
                double dx = Math.Round(imageData.Width / 2 - radius * Math.Sin(radian));
                double dy = Math.Round(imageData.Width / 2 + radius * Math.Cos(radian));
                int nidx = (int)((dx * bmpData.Stride) + dy * 3);

                listData.Add(srcBmp[nidx]);
            }
            imageData.UnlockBits(bmpData);
        }

        public int cvGetProfile(Bitmap image, double angle, out byte[,] profileData)
        {
            int result = -1;
            profileData = null;
            try
            {
                Point startPt, endPt;
                Image<Gray, byte> grayInputImage = new Image<Gray, byte>(image);
                float radian = (float)((angle) * Math.PI / 180);
                double dx = Math.Round(image.Width / 2 - (Math.Abs(image.Width / 2) * Math.Cos(radian)));
                double dy = Math.Round(image.Height / 2 + (Math.Abs(image.Height / 2) * Math.Sin(radian)));

                startPt = new Point((int)dx, (int)dy);

                radian = (float)((angle + 180) * Math.PI / 180);
                dx = Math.Round(image.Width / 2 - (Math.Abs(image.Width / 2) * Math.Cos(radian)));
                dy = Math.Round(image.Height / 2 + (Math.Abs(image.Height / 2) * Math.Sin(radian)));
                endPt = new Point((int)dx, (int)dy);

                LineSegment2D horizontalLine = new LineSegment2D(startPt, endPt);
                profileData = grayInputImage.Sample(horizontalLine, Emgu.CV.CvEnum.Connectivity.EightConnected);
                
                result = 0;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            
            return result;
        }

        */
        #endregion

        #endregion

        #region Hough Line Transform 으로 각도 구하기
        public int Binarization(Bitmap image, out List<Bitmap> listImage)
        {
            int result = -1;
            listImage = new List<Bitmap>();

            do
            {
                try
                {
                    Image<Gray, byte> inputImage = new Image<Gray, byte>(image);
                    Gray value = inputImage.GetAverage();
                    int valueIntensity = (int)value.Intensity;
                    int valueoffset = 255 - (int)value.Intensity;
                    //for (int threshold = valueIntensity - valueoffset; threshold < valueIntensity + valueoffset; threshold+= 10)
                    for (int threshold = 50; threshold < 230; threshold += 10)
                    {
                        Image<Gray, byte> binaryImage = inputImage.ThresholdBinary(new Gray(threshold), new Gray(255));
                        listImage.Add(binaryImage.ToBitmap());
                    }
                }
                catch(Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
            } while (false);

            return result;
        }

        public int HoughLinesTest(List<Bitmap> listImage, string originfileName)
        {
            int result = -1;
            List<double> listDegree = new List<double>();
            double majorAxis = 0;
            do
            {
                try
                {
                    double angle = 0;
                    for (int index = 0; index < listImage.Count; ++index)
                    {
                        Image<Gray, byte> grayInputImage = new Image<Gray, byte>(listImage[index]);
                        //LineSegment2D[][] lines = grayInputImage.HoughLinesBinary(1, Math.PI / 180.0, 30, 50, 30);
                        LineSegment2D[][] lines = grayInputImage.HoughLines(150, 50, 10, Math.PI / 180.0, 40, 50, 20); //test1
                                                                                                                       //LineSegment2D[][] lines = grayInputImage.HoughLines(150, 50, 1, Math.PI / 180.0, 30, 150, 50);//test2

                        LineSegment2D horizontalLine;
                        foreach (var line in lines[0])
                        {
                            grayInputImage.Draw(line, new Gray(150), 2);
                            if (line.P1.Y > line.P2.Y)
                            {
                                horizontalLine = new LineSegment2D(new Point(line.P1.X - 2, line.P1.Y), new Point(line.P2.X, line.P1.Y));
                            }
                            else
                            {
                                horizontalLine = new LineSegment2D(new Point(line.P2.X + 2, line.P2.Y), new Point(line.P1.X, line.P2.Y));
                            }
                            angle = Math.Abs(line.GetExteriorAngleDegree(horizontalLine));
                            listDegree.Add(angle);
                        }
                    }

                    Histogram(listDegree, ref majorAxis);
                    //Add(listDegree, ref majorAxis);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
            } while (false);

            #region 주석(drawline)
            /*
            string filename = originfileName;
            Bitmap img = new Bitmap(filename);
            Graphics gr = Graphics.FromImage(img);
            System.Drawing.Brush br = new SolidBrush(System.Drawing.Color.Black);
            System.Drawing.Pen pen = new System.Drawing.Pen(br, 3);
            System.Drawing.Point startpt = new System.Drawing.Point((int)(img.Width / 2)+5, (int)(img.Height / 2)+5);
            System.Drawing.Point endpt = new System.Drawing.Point();
            endpt.X = (int)(startpt.X + Math.Cos(majorAxis * Math.PI / 180) * (img.Width / 2) + 5);
            endpt.Y = (int)(startpt.Y - Math.Sin(majorAxis * Math.PI / 180) * (img.Height / 2) + 5);
            gr.DrawLine(pen, startpt.X, startpt.Y, endpt.X, endpt.Y);
            */
            //string saveimagepath = //"C:\\Users\\SK\\Desktop\\binary\\7_test3.jpg";
            //img.Save(saveimagepath);
            #endregion
            return result;
        }

        private void Histogram(List<double> list, ref double majorAxis)
        {
            int arrcnt = 180 / 10;
            int[] arrHistogram = new int[arrcnt];
            double[] arrSumHistogram = new double[arrcnt];
            int arrindex = 0;

            for (int index = 0; index < list.Count; ++index)
            {
                if (list[index] == float.NaN) continue;
                if (list[index] >= 180)
                    list[index] = list[index] - 180;

                arrindex = (int)(list[index] / 10);
                arrHistogram[arrindex] += 1;
                arrSumHistogram[arrindex] += list[index];

                if (arrindex == 0)
                {
                    arrindex = arrHistogram.Length;
                    list[index] = list[index] + 180;
                }
                arrHistogram[arrindex - 1] += 1;
                arrSumHistogram[arrindex - 1] += list[index];
            }

            int pickIndex = 0;
            int pick = arrHistogram[pickIndex];
            for (int index = 0; index < arrHistogram.Length; ++index)
            {
                if (arrHistogram[index] >= pick)
                {
                    pickIndex = index;
                    pick = arrHistogram[pickIndex];
                }
            }
            majorAxis = (arrSumHistogram[pickIndex] / arrHistogram[pickIndex]);
            if (majorAxis >= 180) majorAxis -= 180;
        }

        private void Add(List<double> list, ref double majorAxis)
        {
            double sumAngle = 0;
            foreach(var angle in list)
            {
                sumAngle += angle;
            }
            majorAxis = sumAngle / list.Count;
        }
        #endregion
    }
}
