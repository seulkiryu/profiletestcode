﻿namespace WavinessProfileTest
{
    partial class TestForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_makeImage = new System.Windows.Forms.Button();
            this.button_Profile = new System.Windows.Forms.Button();
            this.button_Hough = new System.Windows.Forms.Button();
            this.textBox_degree = new System.Windows.Forms.TextBox();
            this.textBox_imagePath = new System.Windows.Forms.TextBox();
            this.button_selectFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_numLines = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_makeImage
            // 
            this.button_makeImage.Location = new System.Drawing.Point(150, 79);
            this.button_makeImage.Name = "button_makeImage";
            this.button_makeImage.Size = new System.Drawing.Size(95, 23);
            this.button_makeImage.TabIndex = 0;
            this.button_makeImage.Text = "Make Image";
            this.button_makeImage.UseVisualStyleBackColor = true;
            this.button_makeImage.Click += new System.EventHandler(this.button_makeImage_Click);
            // 
            // button_Profile
            // 
            this.button_Profile.Location = new System.Drawing.Point(31, 140);
            this.button_Profile.Name = "button_Profile";
            this.button_Profile.Size = new System.Drawing.Size(94, 23);
            this.button_Profile.TabIndex = 1;
            this.button_Profile.Text = "Profile";
            this.button_Profile.UseVisualStyleBackColor = true;
            this.button_Profile.Click += new System.EventHandler(this.button_Profile_Click);
            // 
            // button_Hough
            // 
            this.button_Hough.Location = new System.Drawing.Point(522, 79);
            this.button_Hough.Name = "button_Hough";
            this.button_Hough.Size = new System.Drawing.Size(97, 23);
            this.button_Hough.TabIndex = 2;
            this.button_Hough.Text = "Hough Test";
            this.button_Hough.UseVisualStyleBackColor = true;
            this.button_Hough.Click += new System.EventHandler(this.button_Hough_Click);
            // 
            // textBox_degree
            // 
            this.textBox_degree.Location = new System.Drawing.Point(231, 142);
            this.textBox_degree.Name = "textBox_degree";
            this.textBox_degree.Size = new System.Drawing.Size(46, 21);
            this.textBox_degree.TabIndex = 3;
            this.textBox_degree.Text = "0";
            // 
            // textBox_imagePath
            // 
            this.textBox_imagePath.Location = new System.Drawing.Point(31, 38);
            this.textBox_imagePath.Name = "textBox_imagePath";
            this.textBox_imagePath.Size = new System.Drawing.Size(588, 21);
            this.textBox_imagePath.TabIndex = 4;
            // 
            // button_selectFile
            // 
            this.button_selectFile.Location = new System.Drawing.Point(31, 79);
            this.button_selectFile.Name = "button_selectFile";
            this.button_selectFile.Size = new System.Drawing.Size(94, 23);
            this.button_selectFile.TabIndex = 5;
            this.button_selectFile.Text = "Load Image";
            this.button_selectFile.UseVisualStyleBackColor = true;
            this.button_selectFile.Click += new System.EventHandler(this.button_selectFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(139, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "Angle(degree)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "Number of lines";
            // 
            // textBox_numLines
            // 
            this.textBox_numLines.Location = new System.Drawing.Point(415, 142);
            this.textBox_numLines.Name = "textBox_numLines";
            this.textBox_numLines.Size = new System.Drawing.Size(52, 21);
            this.textBox_numLines.TabIndex = 8;
            this.textBox_numLines.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 210);
            this.Controls.Add(this.textBox_numLines);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_selectFile);
            this.Controls.Add(this.textBox_imagePath);
            this.Controls.Add(this.textBox_degree);
            this.Controls.Add(this.button_Hough);
            this.Controls.Add(this.button_Profile);
            this.Controls.Add(this.button_makeImage);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_makeImage;
        private System.Windows.Forms.Button button_Profile;
        private System.Windows.Forms.Button button_Hough;
        private System.Windows.Forms.TextBox textBox_degree;
        private System.Windows.Forms.TextBox textBox_imagePath;
        private System.Windows.Forms.Button button_selectFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_numLines;
    }
}

