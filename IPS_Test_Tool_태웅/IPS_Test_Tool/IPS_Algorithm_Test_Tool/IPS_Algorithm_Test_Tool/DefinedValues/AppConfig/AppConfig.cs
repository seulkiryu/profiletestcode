﻿using IPS_Algorithm_Test_Tool.DefinedValues.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS_Algorithm_Test_Tool.DefinedValues.AppConfig
{
    public static class AppConfig
    {
        public static string AnglesSaveDirPath { get { return ConfigurationManager.AppSettings[nameof(AnglesSaveDirPath)]; } }
        public static string ImageFormat { get { return ConfigurationManager.AppSettings[nameof(ImageFormat)]; } }
        public static char[] ImageFileNamingRule_Seperator { get { return ConfigurationManager.AppSettings[nameof(ImageFileNamingRule_Seperator)].ToCharArray(); } }
        public static int ImageFileNamingRule_LotLocation { get { return Convert.ToInt32(ConfigurationManager.AppSettings[nameof(ImageFileNamingRule_LotLocation)]); } }
        public static string FeatureResultFolderName { get { return ConfigurationManager.AppSettings[nameof(FeatureResultFolderName)]; } }
        public static string ImageFileFolderStruct { get { return ConfigurationManager.AppSettings[nameof(ImageFileFolderStruct)]; } }
    }
}
