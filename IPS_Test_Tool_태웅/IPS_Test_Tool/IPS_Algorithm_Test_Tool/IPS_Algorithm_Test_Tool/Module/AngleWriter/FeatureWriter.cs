﻿using IPSAlgorithm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS_Algorithm_Test_Tool.Module.AngleWriter
{
    public class FeatureWriter
    {
        private StringBuilder stringBuilder { get; } = new StringBuilder();
        
        /// <summary>
        /// 각도를 파일에 쓰는 메서드
        /// </summary>
        /// <param name="saveDirPath"></param>
        /// <param name="wafers"></param>
        public async void WriteFeatureToFile(WaferSet wafers, string saveDirPath)   // 묶음 단위 모델링 필요
        {
            string angles = MakeAngleSimilarityString(wafers.wafer);
            if (string.IsNullOrEmpty(angles) == false)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(saveDirPath);
                if (dirInfo.Exists == false)
                    dirInfo.Create();

                FileInfo fileInfo = new FileInfo(Path.Combine(saveDirPath, "FeatureResult.txt"));
                if (fileInfo.Exists)
                    fileInfo.Delete();

                using (var stream = fileInfo.Create())
                {
                    using (var file = new StreamWriter(stream))
                    {
                        var task = file.WriteAsync(angles);
                        await task;
                    }
                }
            }
        }

        private string MakeAngleSimilarityString(IEnumerable<Wafer> wafers)
        {
            stringBuilder.Clear();
            string str = "";

            foreach (var wafer in wafers)
            {
                stringBuilder.AppendLine($"{wafer.filepath} - Angle : {wafer.angle}, Similarity : {wafer.similarity}");
            }

            str = stringBuilder.ToString();
            return str;
        }
    }
}
