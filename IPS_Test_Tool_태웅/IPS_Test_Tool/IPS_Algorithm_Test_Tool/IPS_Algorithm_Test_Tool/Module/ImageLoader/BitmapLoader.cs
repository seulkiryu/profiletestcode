﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IPS_Algorithm_Test_Tool.Module.ImageLoader
{
    public static class BitmapLoader
    {
        public static Bitmap LoadImageFromFile(string fileName)
        {
            Bitmap loadedImage = null;
            try
            {
                using (var stream = File.OpenRead(fileName))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        byte[] buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, Convert.ToInt32(stream.Length));
                        memoryStream.Write(buffer, 0, Convert.ToInt32(stream.Length));
                        loadedImage = (Bitmap)Image.FromStream(memoryStream);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            return loadedImage;
        }
    }
}
