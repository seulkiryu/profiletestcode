﻿using IPS_Algorithm_Test_Tool.DefinedValues.AppConfig;
using IPS_Algorithm_Test_Tool.DefinedValues.Enums;
using IPS_Algorithm_Test_Tool.Module.AngleWriter;
using IPS_Algorithm_Test_Tool.Module.ImageLoader;
using IPSAlgorithm;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace IPS_Algorithm_Test_Tool.Module.Feature
{
    public class FeatureAlgorithmTester
    {
        public int ThreadCount { get { return _ThreadIDs.Count(); } }
        public double Progress { get { var sum = _CompleteCount.Sum(); return sum * 100 / _Total; } }

        private double _Total { get; set; }
        private Action _ThreadCountChanged { get; set; }
        private Action _ProgressChanged { get; set; }
        private ConcurrentDictionary<int?, int> _ThreadIDs { get; } = new ConcurrentDictionary<int?, int>();
        private ConcurrentBag<int> _CompleteCount { get; set; }

        public void RunFeatureAlgorithmTest(string path, Action threadAction, Action progressAction)
        {
            var dirInfo = new DirectoryInfo(path);
            if (dirInfo.Exists)
            {
                _ThreadIDs.Clear();
                _CompleteCount = new ConcurrentBag<int>();
                _Total = dirInfo.GetFiles("*jpg", SearchOption.AllDirectories).Count();
                _ThreadCountChanged = threadAction;
                _ProgressChanged = progressAction;

                MakeWaferSetAndRun(dirInfo);
            }
        }

        private void MakeWaferSetAndRun(DirectoryInfo dirInfo)
        {
            var imageFiles = dirInfo.GetFiles().Where(w => w.Extension.ToLower() == AppConfig.ImageFormat.ToLower());
            if (imageFiles.Count() > 0)
            {
                string folderStruct = AppConfig.ImageFileFolderStruct;
                if (folderStruct == "Virtual")
                    MakeWaferSetAndRun_Virtual(dirInfo, imageFiles);
                else if (folderStruct == "Real")
                    MakeWaferSetAndRun_Real(dirInfo, imageFiles);
            }

            var dirInfos = dirInfo.GetDirectories().Where(w => w.Name.ToUpper() != AppConfig.FeatureResultFolderName.ToUpper());
            if (dirInfos.Count() > 0)
            {
                Parallel.ForEach(dirInfos, (dir) =>
                {
                    MakeWaferSetAndRun(dir);
                });
            }
        }

        private void MakeWaferSetAndRun_Virtual(DirectoryInfo dirInfo, IEnumerable<FileInfo> imageFiles)  //Test 해봄
        {
            //Make WaferSet
            var waferSet = new WaferSet();
            IEnumerable<Wafer> lotImages = null;
            Parallel.ForEach(imageFiles, (file) =>
            {
                lotImages = imageFiles.Select(s => new Wafer()
                {
                    filepath = s.FullName,
                    image = BitmapLoader.LoadImageFromFile(s.FullName)
                });
            });
            waferSet.wafer = new List<Wafer>(lotImages);

            //Make Result Folder Path
            var featureResultFolderPath = Path.Combine(dirInfo.FullName, AppConfig.FeatureResultFolderName);
            //Run
            RunAlgorithm(waferSet, featureResultFolderPath);
        }

        private void MakeWaferSetAndRun_Real(DirectoryInfo dirInfo, IEnumerable<FileInfo> imageFiles)
        {
            int lotLocation;
            lotLocation = AppConfig.ImageFileNamingRule_LotLocation;
            var lotIDs = imageFiles.AsParallel().Select(s => s.Name.Split(AppConfig.ImageFileNamingRule_Seperator)[lotLocation]).Distinct();
            Parallel.ForEach(lotIDs, (lotID) =>
            {
                var lotImages = imageFiles.Where(w => w.Name.Contains(lotID))
                    .Select(s => new Wafer()
                    {
                        filepath = s.FullName,
                        image = BitmapLoader.LoadImageFromFile(s.FullName)
                    });
                var waferSet = new WaferSet() { wafer = new List<Wafer>(lotImages) };

                //Make Result Folder Path
                var featureResultFolderPath = Path.Combine(dirInfo.FullName, AppConfig.FeatureResultFolderName, lotID);

                //Run
                RunAlgorithm(waferSet, featureResultFolderPath);
            });
        }

        private async void RunAlgorithm(WaferSet wafers, string savePath)
        {
            int thread = 0;

            Task<int?> task = Task<int?>.Factory.StartNew(() =>
            {
                _ThreadIDs.TryAdd(Task.CurrentId, thread);
                _ThreadCountChanged?.Invoke();
                //Feature(wafers);
                Thread.Sleep(10000);
                return Task.CurrentId;
            });

            var id = await task;

            FeatureWriter writer = new FeatureWriter();
            writer.WriteFeatureToFile(wafers, savePath);

            _ThreadIDs.TryRemove(id, out thread);
            _CompleteCount.Add(wafers.wafer.Count());

            _ThreadCountChanged?.Invoke();
            _ProgressChanged?.Invoke();
        }
    }
}
