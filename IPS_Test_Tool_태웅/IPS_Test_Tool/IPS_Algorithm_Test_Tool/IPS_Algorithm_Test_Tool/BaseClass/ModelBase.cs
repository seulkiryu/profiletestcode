﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS_Algorithm_Test_Tool.BaseClass
{
    public class ModelBase : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public virtual object Clone()
        {
            throw new NotImplementedException("ModelBase.Clone - NotImplemented !!!!");
        }
    }
}
