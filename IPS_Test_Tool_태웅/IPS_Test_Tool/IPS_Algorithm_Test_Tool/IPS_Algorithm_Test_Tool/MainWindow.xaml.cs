﻿using IPS_Algorithm_Test_Tool.DefinedValues.AppConfig;
using IPS_Algorithm_Test_Tool.Module.AngleWriter;
using IPS_Algorithm_Test_Tool.Module.Feature;
using IPSAlgorithm;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace IPS_Algorithm_Test_Tool
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public static readonly ILog Logger = LogManager.GetLogger("MainWindow");
        public event PropertyChangedEventHandler PropertyChanged;
        private string _DirPath { get; set; } = "";
        private FeatureAlgorithmTester _FeatureTester { get; set; }
        private Action _ThreadCounterChanged { get; set; }
        private Action _ProgressChanged { get; set; }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            Button_DirSelect.Click += Button_DirSelect_Click;
            Button_Start.Click += Button_Start_Click;
            _FeatureTester = new FeatureAlgorithmTester();
            _ThreadCounterChanged = new Action(() => 
            {
                Label_RunningThreadCount.Content = _FeatureTester.ThreadCount;
            });
            _ProgressChanged = new Action(() => 
            {
                Label_Progress.Content = _FeatureTester.Progress;
            });
        }

        private void Button_Start_Click(object sender, RoutedEventArgs e)
        {
            Logger.Info($"[{MethodBase.GetCurrentMethod()}] Start Button Click - Dir : {_DirPath}");
            _FeatureTester.RunFeatureAlgorithmTest(_DirPath, new Action(UpdateProgress), new Action(UpdateThreadCount));
        }

        private void Button_DirSelect_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _DirPath = dialog.SelectedPath;
                Button_Start.IsEnabled = true;
                Logger.Info($"[{MethodBase.GetCurrentMethod()}] Selected Dir : {_DirPath}");
            }
        }

        private void UpdateProgress()
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, _ProgressChanged);
        }

        private void UpdateThreadCount()
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, _ThreadCounterChanged);
        }
    }
}
